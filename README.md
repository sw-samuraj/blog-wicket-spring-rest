# Wicket + Spring & WebSockets #

An example project for demonstration of:

 * The *Wicket* and *Spring* co-existence.
 * Adding a "reactive-like" behavior to *Wicket* via *WebSockets*.

## How to run the *Wicket & Spring* example ##

 1. Clone the repository.
 1. Run the command `gradle tomcatRun` (`gradle jettyRun` works as well).
 1. Open a browser on URL <http://localhost:4040/deep-thought> to see the *Wicket* stuff.
 1. Open a browser on URL <http://localhost:4040/deep-thought/question> to see the *Spring* stuff.
 1. **Browse the source code.**

## How to run the *Wicket & WebSockets* example ##

 1. Clone the repository.
 1. Run the command `gradle tomcatRun` (no *Jetty*!).
 1. Open a browser on URL <http://localhost:4040/deep-thought> to see a page without data.
 1. Open *SoapUI* project from `src/test/soapui`.
 1. Run any request from the  *putPerson* method.
 1. Check the browser with newly loaded data.
 1. Check the log in the console.
 1. **Browse the source code.**

## License ##


The **blog-wicket-spring-rest** project is published under [BSD 3-Clause](http://opensource.org/licenses/BSD-3-Clause) license.
