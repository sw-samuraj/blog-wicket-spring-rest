package cz.swsamuraj.wicketspring;

import org.apache.wicket.protocol.ws.api.message.IWebSocketPushMessage;

import java.time.OffsetDateTime;

public class WebSocketPersonMessage implements IWebSocketPushMessage {

    private String personID;

    public WebSocketPersonMessage(String personID) {
        this.personID = personID;
    }

    public String getPersonID() {
        return personID;
    }

    @Override
    public String toString() {
        return "Person ID: " + personID;
    }
}
