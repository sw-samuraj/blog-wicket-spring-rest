package cz.swsamuraj.wicketspring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebMvc
@ComponentScan("cz.swsamuraj.wicketspring")
public class SpringRestConfiguration {

    @Bean
    public ObservableCache observableCache() {
        return new ObservableCache();
    }
}
