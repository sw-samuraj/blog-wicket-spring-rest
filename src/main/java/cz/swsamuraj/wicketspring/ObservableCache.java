package cz.swsamuraj.wicketspring;

import cz.swsamuraj.wicketspring.ws.model.PersonInfo;

import java.util.HashMap;
import java.util.Map;
import java.util.Observable;

public class ObservableCache extends Observable {

    private Map<String, PersonInfo> cache = new HashMap<>();

    public void put(String key, PersonInfo person) {
        cache.put(key, person);
        setChanged();
        notifyObservers();
    }

    public boolean containsKey(String key) {
        return cache.containsKey(key);
    }

    public PersonInfo get(String key) {
        return cache.get(key);
    }

    public PersonInfo remove(String key) {
        PersonInfo person = cache.remove(key);
        setChanged();
        notifyObservers();

        return person;
    }

}
