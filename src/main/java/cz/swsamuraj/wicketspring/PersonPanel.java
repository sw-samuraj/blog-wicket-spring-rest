package cz.swsamuraj.wicketspring;

import cz.swsamuraj.wicketspring.ws.model.PersonInfo;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.wicket.event.IEvent;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.protocol.ws.WebSocketSettings;
import org.apache.wicket.protocol.ws.api.WebSocketBehavior;
import org.apache.wicket.protocol.ws.api.WebSocketPushBroadcaster;
import org.apache.wicket.protocol.ws.api.event.WebSocketPushPayload;
import org.apache.wicket.protocol.ws.api.message.ConnectedMessage;
import org.apache.wicket.protocol.ws.api.message.IWebSocketPushMessage;
import org.apache.wicket.spring.injection.annot.SpringBean;

import java.util.Observable;
import java.util.Observer;

public class PersonPanel extends Panel implements Observer {

    private static final Logger logger = LogManager.getLogger(PersonPanel.class);

    @SpringBean
    private ObservableCache observableCache;

    private WebMarkupContainer wrapper;

    private String personID;

    public PersonPanel(String id, String personID) {
        super(id);

        this.personID = personID;

        add(new Label("personIDHeader", "Person #" + personID));

        setDefaultModel(new CompoundPropertyModel<>(getModel()));

        wrapper = new WebMarkupContainer("wrapper");
        wrapper.setOutputMarkupId(true);
        add(wrapper);

        wrapper.add(new Label("personID"));
        wrapper.add(new Label("firstName"));
        wrapper.add(new Label("lastName"));
        wrapper.add(new Label("sex"));
        wrapper.add(new Label("timestamp"));

        add(new WebSocketBehavior() {
            @Override
            protected void onConnect(ConnectedMessage message) {
                logger.debug("WebSocket client connected.");
            }
        });

        observableCache.addObserver(this);
    }

    private IModel<PersonInfo> getModel() {
        return new LoadableDetachableModel<PersonInfo>() {
            @Override
            protected PersonInfo load() {
                if (observableCache.containsKey(personID)) {
                    return observableCache.get(personID);
                } else {
                    return new PersonInfo();
                }
            }
        };
    }

    @Override
    public void onEvent(IEvent<?> event) {
        logger.debug("Person Panel #{} received an Event: {}", personID, event.getPayload());

        if (event.getPayload() instanceof WebSocketPushPayload) {
            WebSocketPushPayload wsEvent = (WebSocketPushPayload) event.getPayload();
            IWebSocketPushMessage message = wsEvent.getMessage();

            if (message instanceof WebSocketPersonMessage) {
                WebSocketPersonMessage personMessage = (WebSocketPersonMessage) message;

                if (personID.equals(personMessage.getPersonID())) {
                    logger.debug("Panel #{} will be re-rendered.", personID);

                    wsEvent.getHandler().add(wrapper);
                }
            }
        }
    }

    @Override
    public void update(Observable observable, Object o) {
        logger.debug("Observable has changed, update has been called on Panel #{}", personID);

        if (observableCache.containsKey(personID)) {
            logger.debug("Observable cache contains the key: {}", personID);

            WebSocketSettings webSocketSettings = WebSocketSettings.Holder.get(getApplication());
            WebSocketPushBroadcaster broadcaster = new WebSocketPushBroadcaster(webSocketSettings.getConnectionRegistry());
            broadcaster.broadcastAll(getApplication(), new WebSocketPersonMessage(personID));
        }
    }

}
