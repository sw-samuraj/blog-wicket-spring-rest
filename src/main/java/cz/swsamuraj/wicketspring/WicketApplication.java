package cz.swsamuraj.wicketspring;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.wicket.Page;
import org.apache.wicket.Session;
import org.apache.wicket.event.IEvent;
import org.apache.wicket.protocol.http.WebApplication;
import org.apache.wicket.protocol.http.WebSession;
import org.apache.wicket.request.Request;
import org.apache.wicket.request.Response;
import org.apache.wicket.spring.injection.annot.SpringComponentInjector;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

public class WicketApplication extends WebApplication {

    private static final Logger logger = LogManager.getLogger(WicketApplication.class);

    @Override
    public Class<? extends Page> getHomePage() {
        return HomePage.class;
    }

    @Override
    protected void init() {
        super.init();

        WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext());
        getComponentInstantiationListeners().add(new SpringComponentInjector(this, ctx));
    }

    @Override
    public void onEvent(IEvent<?> event) {
        logger.debug("Application received an Event: {}", event.getPayload());
    }

    @Override
    public Session newSession(Request request, Response response) {
        return new WebSession(request) {
            @Override
            public void onEvent(IEvent<?> event) {
                logger.debug("Session received an Event: {}", event.getPayload());
            }
        };
    }

}
