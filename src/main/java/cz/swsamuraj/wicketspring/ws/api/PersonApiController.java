package cz.swsamuraj.wicketspring.ws.api;

import cz.swsamuraj.wicketspring.ObservableCache;
import cz.swsamuraj.wicketspring.ws.model.PersonInfo;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;

@Controller
public class PersonApiController implements PersonApi {

    private static final Logger logger = LogManager.getLogger(PersonApiController.class);

    @Autowired
    private ObservableCache observableCache;

    @Override
    public ResponseEntity<Void> putPerson(@RequestBody PersonInfo person) {
        logger.debug("PersonInfo:\n" + person);

        observableCache.put(person.getPersonID(), person);

        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }
}
