package cz.swsamuraj.wicketspring;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.wicket.event.IEvent;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;

public class HomePage extends WebPage {

    private static final Logger logger = LogManager.getLogger(HomePage.class);

    public HomePage() {
        add(new Label("helloLabel", "Hello, Wicket & Spring!"));
        add(new PersonPanel("personPanel42", "42"));
        add(new PersonPanel("personPanel12", "12"));
    }

    @Override
    public void onEvent(IEvent<?> event) {
        logger.debug("Page received an Event: {}", event.getPayload());
    }
}
